# ShowLayer script - Show current layer on the controller
#
# This script is a quick migration of the ShowLayer plugin for legacy Cura.
# 
# This script is licensed under the Creative Commons - Attribution - Share Alike (CC BY-SA) terms
# Original Author: Philippe Vanhaesendonck 
# Modifié par Olivier Testault
from UM.Application import Application #To get the current printer's settings.
from ..Script import Script
class ShowLayer(Script):
    def __init__(self):
        super().__init__()
        
    def getSettingDataString(self):
        return """{ 
            "name": "Show Layer",
            "key": "ShowLayer",
            "metadata": {},
            "version": 2,
            "settings": {}
        }"""
    
    def execute(self, data):
        initial_layer_height = Application.getInstance().getGlobalContainerStack().getProperty("layer_height_0", "value")
        layer_0_z = 0
        current_z = 0
        current_height = 0
        current_layer = 0
        got_first_g_cmd_on_layer_0 = False
        count = "-"
        for index, layer in enumerate(data):
            new_layer = ""
            lines = layer.split("\n")
            for line in lines:
                if line:
                    new_layer += line + "\n"
                    
                    # If a Z instruction is in the line, read the current Z
                    if self.getValue(line, "Z") is not None:
                        current_z = self.getValue(line, "Z")

                    if line.startswith(";LAYER_COUNT:"):
                        count = line[13:].rstrip()
                    if line.startswith(";LAYER:"):
                        this_layer = int(line[7:].rstrip()) + 1
                        new_layer += "M117 Couche {0}/{1} Z=".format(this_layer, count) + str(current_z) +"mm\n"
            data[index] = new_layer
        return data
