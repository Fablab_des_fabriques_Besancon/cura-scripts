Quelques scripts en python pour Cura :
 - MusicalPause.py : une version modifié du PauseAtHeight qui ajoute la possibilité de jouer une musique au début de la pause
 - ShowLayer.py : affiche un suivi de la couche imprimée et de la valeur Z de la buse sur l'écran pendant l'impression
